from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^orders/$', views.get_post_orders, name = 'get_post_orders'),
    url(r'^orders/(?P<pk>[0-9]+)/$', views.get_delete_update_orders, name = 'get_delete_update_orders')
]
