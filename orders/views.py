# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from .models import Orders
from .serializers import OrderSerializer

from django.shortcuts import render


@api_view(['GET', 'POST'])
def get_post_orders(request):
    """
    List all orders, or create a new order.
    """
    if request.method == 'GET':
        orders = Orders.objects.all()
        serializer = OrderSerializer(orders, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
		serializer = OrderSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def get_delete_update_orders(request, pk):
    """
    Get, udpate, or delete a specific order
    """
    try:
        orders = Orders.objects.get(pk=pk)
    except Orders.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = OrderSerializer(orders)
        return Response(serializer.data)

    elif request.method == 'PUT':
		serializer = OrderSerializer(orders, data=data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        oder.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
