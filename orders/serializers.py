from .models import Orders
from rest_framework import serializers

class OrderSerializer(serializers.ModelSerializer):
	class Meta:
		model = Orders
		fields = ('id', 'pizza_id', 'pizza_size', 'cost_name', 'cost_adress', 'created_at', 'updated_at')
	
	def create(self, validated_data):
		return Orders.objects.create(**validated_data)
		
	def update(self, instance, validated_data):
		instance.pizza_id = validated_data.get('pizza_id', instance.pizza_id)
		instance.pizza_size = validated_data.get('pizza_size', instance.pizza_size)
		instance.cost_name = validated_data.get('cost_name', instance.cost_name)
		instance.cost_adress = validated_data.get('cost_adress', instance.cost_adress)
		instance.save()
		return instance
