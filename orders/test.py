import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from .models import Orders
from rest_framework.decorators import api_view
from .serializers import OrderSerializer

# initialize the APIClient app
client = Client()


class GettAllOrdersTest(TestCase):
	""" Test module for Orders model """
	
	def setUp(self):
		Orders.objects.create(pizza_id = 4, pizza_size = 30, cost_name = 'Sabine Koenig', cost_adress='Friedrichstrasse 10, 26673 Berlin')
		Orders.objects.create(pizza_id = 13, pizza_size = 30, cost_name = 'Dennis Reuter', cost_adress='Marienplatz 4, 16782 Berlin')
		Orders.objects.create(pizza_id = 5, pizza_size = 50, cost_name = 'Thomas Engel', cost_adress='Ehrenstrasse 1, 37790 Berlin')
		Orders.objects.create(pizza_id = 2, pizza_size = 30, cost_name = 'Hans Kleist', cost_adress='Pettenkoferstrasse 1, 18763 Berlin')
		
		
	def test_get_all_puppies(self):
		# get API response
		response = client.get(reverse('get_post_orders'))
		# get data from db
		orders = Orders.objects.all()
		serializer = OrderSerializer(orders, many=True)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		
		
class GetSingleOrderTest(TestCase):
	""" Test module for GET single order API """
	
	def setUp(self):
		self.sabine = Orders.objects.create(pizza_id = 4, pizza_size = 30, cost_name = 'Sabine Koenig', cost_adress='Friedrichstrasse 10, 26673 Berlin')
		self.dennis = Orders.objects.create(pizza_id = 13, pizza_size = 30, cost_name = 'Dennis Reuter', cost_adress='Marienplatz 4, 16782 Berlin')
		self.thomas = Orders.objects.create(pizza_id = 5, pizza_size = 50, cost_name = 'Thomas Engel', cost_adress='Ehrenstrasse 1, 37790 Berlin')
		self.hans = Orders.objects.create(pizza_id = 2, pizza_size = 30, cost_name = 'Hans Kleist', cost_adress='Pettenkoferstrasse 1, 18763 Berlin')
		
	def test_get_valid_single_order(self):
		response = client.get(reverse('get_delete_update_orders', kwargs={'pk': self.dennis.pk}))
		orders = Orders.objects.get(pk=self.dennis.pk)
		serializer = OrderSerializer(orders)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		
		
	def test_get_invalid_single_order(self):
		response = client.get(reverse('get_delete_update_orders', kwargs={'pk': 23}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
