# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Orders(models.Model):
	pizza_id = models.IntegerField()
	pizza_size = models.IntegerField()
	cost_name = models.CharField(max_length=100)
	cost_adress = models.CharField(max_length=255)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	

